# Graphic Project Cookiecutter #

Simple Cookiecutter for basic graphic project.

This is the current structure it create:

- `references` folder to put your references files in
- `images` is where you put your images to be used in your project
- `export` is where you save your completed work ready to be published
  the content of this folder is not tracked


# Usage

Once the project has been created inizialize the repository with:

    git init

And then install the Git Large File Storage (LFS) with:

    git lfs install

Then work as usual.
